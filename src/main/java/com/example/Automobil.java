package com.example;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;


@Entity
@Table(name = "automobili")
public class Automobil extends PanacheEntity {

    @Column(name = "marka")
    private String marka;

    @Column(name = "model")
    private String model;

    @Column(name = "godina_proizvodnje")
    private int godinaProizvodnje;

    @Column(name = "boja")
    private String boja;

    @Column(name = "broj_vrata")
    private int brojVrata;

    @Column(name = "snaga_motora")
    private int snagaMotora;

    @Column(name = "zapremina_motora")
    private int zapreminaMotora;

    @Column(name = "gorivo")
    private String gorivo;

    @Column(name = "cena")
    private double cena;

    @Column(name = "na_servisu")
    private int naServisu;


    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getGodinaProizvodnje() {
        return godinaProizvodnje;
    }

    public void setGodinaProizvodnje(int godinaProizvodnje) {
        this.godinaProizvodnje = godinaProizvodnje;
    }

    public String getBoja() {
        return boja;
    }

    public void setBoja(String boja) {
        this.boja = boja;
    }

    public int getBrojVrata() {
        return brojVrata;
    }

    public void setBrojVrata(int brojVrata) {
        this.brojVrata = brojVrata;
    }

    public int getSnagaMotora() {
        return snagaMotora;
    }

    public void setSnagaMotora(int snagaMotora) {
        this.snagaMotora = snagaMotora;
    }

    public int getZapreminaMotora() {
        return zapreminaMotora;
    }

    public void setZapreminaMotora(int zapreminaMotora) {
        this.zapreminaMotora = zapreminaMotora;
    }

    public String getGorivo() {
        return gorivo;
    }

    public void setGorivo(String gorivo) {
        this.gorivo = gorivo;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public int getNaServisu() {
        return naServisu;
    }

    public void setNaServisu(int naServisu) {
        this.naServisu = naServisu;
    }
}