package com.example;

import io.micrometer.core.annotation.Counted;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.Collection;

@Path("/autokuca")
@Transactional(Transactional.TxType.REQUIRES_NEW)
public class AutomobilResource {

    @Inject
    MeterRegistry meterRegistry;
    private Counter mojCounter;
    @PostConstruct
    public void init() {
        mojCounter = meterRegistry.counter("endpoint_broj_automobila_count_manual");
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/automobili")
    public Collection<Automobil> getCars() {
        return Automobil.findAll().list();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/broj_automobila")
    @Counted("endpoint_broj_automobila_count")
    public long carCount() {
        mojCounter.increment();
        return Automobil.findAll().stream().count();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dodaj_automobil")
    public Automobil createCar(Automobil automobil) {
        // Persist the new car
        automobil.persist();
        return automobil;
    }

    @DELETE
    @Path("/obrisi_automobil/{id}")
    public String deleteCar(@PathParam("id") long id) {
        Automobil carToDelete = Automobil.findById(id);
        if (carToDelete == null) {
            throw new NotFoundException("Car with ID " + id + " not found");
        }

        // Delete the car
        carToDelete.delete();
        return "Car with ID " + id + " deleted successfully.";
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/azuriraj_automobil/{id}")
    public Automobil updateCar(@PathParam("id") long id, Automobil updatedCar) {
        Automobil existingCar = Automobil.findById(id);
        if (existingCar == null) {
            throw new NotFoundException("Car with ID " + id + " not found");
        }
        existingCar.setMarka(updatedCar.getMarka());
        existingCar.setModel(updatedCar.getModel());
        existingCar.setGodinaProizvodnje(updatedCar.getGodinaProizvodnje());
        existingCar.setBoja(updatedCar.getBoja());
        existingCar.setBrojVrata(updatedCar.getBrojVrata());
        existingCar.setSnagaMotora(updatedCar.getSnagaMotora());
        existingCar.setZapreminaMotora(updatedCar.getZapreminaMotora());
        existingCar.setGorivo(updatedCar.getGorivo());
        existingCar.setCena(updatedCar.getCena());
        existingCar.setNaServisu(updatedCar.getNaServisu());

        // Persist the updated car
        existingCar.persist();
        return existingCar;
    }
}
